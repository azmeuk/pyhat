from quart import Quart
from quart import render_template

app = Quart(__name__)


@app.route("/")
async def hello():
    return await render_template("hello.html")


if __name__ == "__main__":  # pragma: no cover
    app.run(debug=True)
