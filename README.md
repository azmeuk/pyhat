# pyhat

Exemple of python application using:

- quart
- tailwind css
- htmx
- python-dotenv
- authlib

- pytest
- tox
- pre-commit

The `poetry` and `tox` binaries are needed to develop the project.

# Developping

```
poetry install
```

# Testing

```
tox
```
