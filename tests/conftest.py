import pytest
from pyhat import app


@pytest.fixture
def testclient():
    return app.test_client()
