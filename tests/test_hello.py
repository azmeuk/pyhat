import pytest


@pytest.mark.asyncio
async def test_app(testclient):
    response = await testclient.get("/")
    assert response.status_code == 200
